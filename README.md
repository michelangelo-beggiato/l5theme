# Laravel 5 Theme

Semplice theme manager

## Installazione
```php
php composer require beggiatom/theme
```
Aggiungere il service provider in config/app.php
```php
providers => [
	...
    Beggiatom\Theme\Providers\ThemeServiceProvider::class,
    ...
]
```
Aggiungere il facade in config/app.php
```php
aliases => [
	...
    'Theme'     => Beggiatom\Theme\Facades\ThemeFacade::class,
    ...
]
```
Pubblicare il file di configurazione e il tema di default
```php
php artisan vendor:publish
```
## Utilizzo
Estendere il proprio controller con \Beggiatom\Theme\Controllers\ThemeController
```php
<?php

    namespace App\Http\Controllers;
    
    use Beggiatom\Theme\Controllers\ThemeController;

    class MyController extends ThemeController
    {

        public function __construct() {
            parent::__construct();
        }

    }
```
## Metodo disponibile nel controller
```php
$this->response($view, $param = [], $layout = null);
```
### Parametri
**$view** vista da mostrare. Es. home.index (resources/views/home/index.blade.php)

**$param** parametri da passare alla vista
**$layout** layout da utilizzare per la vista
## Vista
La vista dovrà estendere il layout
```php
@extend($theme_layout)

@section('content')
	...
@endsection
```
## Configurazione
Il file di configurazione (config/theme.php) presente alcuni parametri che è possibile modificare a proprio piacimento
```php
<?php

    return [
        'default' => [
            'theme' => 'default', // tema utilizzato di default
            'layout' => 'default' // layout utilizzato di default
        ],
        'paths' => [
            'themes' => 'themes', // cartella dove sono presenti i temi
            'layouts' => 'layouts', // cartella dove sono presenti i layouts, questa è relativa a "themes"
            'assets' => 'assets', // cartella dove sono presenti gli assets del tema, questa è relativa a "themes"
            'partials' => 'partials', // cartella dove sono presenti possibili componenti del layout, questa è relativa a "layouts"
        ]
    ];
```
## Layouts
Il layout è un involucro del contenuto della pagina. Richiamato nella vista con
```php
@extend($theme_layout)
```
## Assets
Per caricare i file css e js nel layout è possibili utilizzare il metodo ::Assets
Esempio per richiamare lo stylesheet di bootstrap basta inserire nel layout tra gli HEAD
```php
{!! Theme::Assets('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') !!}
```
oppure
```php
{!! Theme::Assets('css/bootstrap.min.css') !!}
```
in questo caso il file verrà cercato nella cartella assets/css/bootstrap.min.css
E' possibile invece che utilizzare il nome di un file direttamente, utilizzare una puntatore del tipo "bootstrap-css", quando indicato in questo modo il sistema andrà alla ricerca all'interno di un array posto nel file assets.json, il quale è collocato nella cartella degli assets del tema, della chiave "boostrap-css", se trovata verrà caricata il valore della chiave.
Esempio del file nomeTema/assets/assets.json
```json
{
  "jquery": "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js",
  "bootstrap-css": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
  "bootstrap-js": "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
}
```
Al posto di una stringa è possibile passare un array 
```php
{!! Theme::Assets(['jquery','bootstrap-js']) !!}
```
in questo modo verranno caricati tutti gli assets presenti nell'array senza doverli richiamare uno ad uno.
## Partials
All'interno di un layout potrebbero essere presenti delle strutture che si ripetono in due o più layouts in questo case è possibile richiamarli con il metodo
```php
{!! Theme::partials('nomeDelPartial', $param = []) !!}
```
Questo verrà incluso nel layout dove è stato richiamato
