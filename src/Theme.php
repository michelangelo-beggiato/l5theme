<?php

    namespace Beggiatom\Theme;

    use Illuminate\Http\Request;
    use Illuminate\Support\Collection;
    use Illuminate\View\Factory as ViewFactory;
    use Illuminate\Config\Repository as Config;

    /**
     * Class Theme
     * @package Beggiatom\Theme
     */
    class Theme
    {

        /**
         * @var
         */
        protected $theme;
        /**
         * @var
         */
        protected $layout;
        /**
         * @var ViewFactory
         */
        protected $viewFactory;
        /**
         * @var Config
         */
        protected $config;
        /**
         * @var Request
         */
        protected $request;

        protected $requireJs = [];

        /**
         * Template utilizzato per gli stylesheet
         */
        const TEMPLATE_CSS = '<link rel="stylesheet" type="text/css" href="%s"%s>';

        /**
         * Template utilizzato per gli script javascript
         */
        const TEMPLATE_JS = '<script src="%s"%s></script>';

        /**
         * @param ViewFactory $viewFactory
         * @param Config $config
         * @param Request $request
         */
        public function __construct(ViewFactory $viewFactory, Config $config, Request $request)
        {
            $this->viewFactory = $viewFactory;
            $this->config = $config;
            $this->request = $request;
        }

        /**
         * @param null $theme
         * @return $this
         */
        public function setTheme($theme = null)
        {
            $this->theme = is_null($theme) ? $this->config->get('theme.default.theme') : $theme;
            return $this;
        }

        /**
         * Imposto il layout della pagina
         * Se il layout non è stato specificato come parametro lo cerco nel file site.json se anche li non è
         * stato impostato cerco nel file di configurazione
         *
         * @param null $layout
         * @return $this
         */
        public function setLayout($layout = null)
        {
            if (is_null($layout)) {
                $this->layout = $this->trySetLayoutBySite($this->config->get('theme.default.layout'));
            } else {
                $this->layout = $layout;
            }
            return $this;
        }

        /**
         * @return mixed
         */
        public function getTheme()
        {
            return $this->theme;
        }

        /**
         * @return mixed
         */
        public function getLayout()
        {
            return $this->layout;
        }

        /**
         * @return string
         */
        protected function getPathTheme()
        {
            return public_path($this->config->get('theme.paths.themes')) . DIRECTORY_SEPARATOR . $this->theme;
        }

        /**
         * @return string
         */
        protected function getPathLayout()
        {
            return $this->getPathTheme() . DIRECTORY_SEPARATOR . $this->config->get('theme.paths.layouts');
        }

        /**
         * @return string
         */
        protected function getPathPartials()
        {
            return $this->getPathLayout() . DIRECTORY_SEPARATOR . $this->config->get('theme.paths.partials');
        }

        /**
         * @return string
         */
        protected function getAssetsTheme()
        {
            return $this->getPathTheme() . DIRECTORY_SEPARATOR . $this->config->get('theme.paths.assets');
        }

        /**
         * @return string
         */
        protected function getHttpAssetsTheme()
        {
            return DIRECTORY_SEPARATOR . $this->config->get('theme.paths.themes') . DIRECTORY_SEPARATOR . $this->config->get('theme.default.theme') . DIRECTORY_SEPARATOR . $this->config->get('theme.paths.assets');
        }

        /**
         * @return string
         */
        protected function getAssetsPublic()
        {
            return public_path('assets');
        }

        /**
         * @return array|mixed
         */
        protected function getJsonAssetsTheme()
        {
            $json = [];
            $file = $this->getAssetsTheme() . DIRECTORY_SEPARATOR . 'assets.json';
            if (\File::exists($file)) {
                $json = json_decode(\File::get($file), true);
            }
            return $json;
        }

        /**
         * @return array|mixed
         */
        protected function getJsonAssetsPublic()
        {
            $json = [];
            $file = public_path('assets') . DIRECTORY_SEPARATOR . 'assets.json';
            if (\File::exists($file)) {
                $json = json_decode(\File::get($file), true);
            }
            return $json;
        }

        /**
         * Controller se esiste il file site.json nella root della cartella dei temi che identifica il
         * layout da utilizzare per la pagina richiesta
         *
         * @return Collection
         */
        protected function getSitesLayouts()
        {
            $siteLayouts = new Collection($this->config->get('theme.layouts'));
//            $jsonAddress = public_path($this->config->get('theme.paths.themes')).DIRECTORY_SEPARATOR.'site.json';
//            if (\File::exists($jsonAddress)) {
//                $json = json_decode(\File::get($jsonAddress), true);
//                $siteLayouts = new Collection($json);
//            }
            return $siteLayouts;
        }

        /**
         * Cerco se esiste una chiave uguale all'URI richiamato
         * Se la chiave non viene trovata viene impostato il layout passato come parametro
         *
         * @param string $default
         * @return mixed
         */
        protected function trySetLayoutBySite($default)
        {
            $siteLayouts = $this->getSitesLayouts();
//            dd($this->request->route()->getPath());
//            return $siteLayouts->get($this->request->getRequestUri(), function() use ($default) {
//                return $default;
//            });
            return $siteLayouts->get($this->request->route()->getPath(), function () use ($default) {
                return $default;
            });
        }

        /**
         * Theme::Assets(array|string)
         * Se è un array: controllo ogni item e in base all'estensione ritorno il giusto template
         * Se è una stringa: ritorno il template in base all'estensione
         * Se l'estensione non è presente, controllo se il file assets.json è presente nella cartella
         * nomeTema/assets e in base alla chiave carico il file indicato
         * @param $file
         * @param array $param
         * @return string
         */
        public function Assets($file, $param = [])
        {

            if (is_array($file)) {
                return $this->parseArrayAssets($file);
            }

            $ext = pathinfo($file, PATHINFO_EXTENSION);

            switch ($ext) {
                case 'js':
                    return $this->renderAssetsJs($file, $param);
                    break;
                case 'css':
                    return $this->renderAssetsCss($file, $param);
                    break;
                default:
                    return $this->renderAssetsJson($file, $param);
                    break;
            }
        }

        /**
         * Parse di ogni item presente nell'array
         *
         * @param $file
         * @return string
         */
        protected function parseArrayAssets($file)
        {
            $html = '';
            foreach ($file as $item) {
                $html .= $this->Assets($item);
            }
            return $html;
        }

        /**
         * Utilizzo il template per ritornare la stringa html per gli stylesheets
         *
         * @param $file
         * @param array $param
         * @return string
         */
        protected function renderAssetsCss($file, $param = [])
        {
            $html = '';
            $url = $this->getAssetsTheme();
            starts_with($file, ['http', 'https', '\\']) ? $url = null : null;
            starts_with($file, ['/']) ? $url = $this->getHttpAssetsTheme() : null;
            $tag = '';
            if (count($param)) {
                $totale = count($param);
                $n = 0;
                $tag .= ' ';
                foreach ($param as $key => $value) {
                    $tag .= $key . '="' . $value . '"';
                    $n++;
                    $n < $totale ? $tag .= ' ' : null;
                }
            }
            $html .= sprintf(self::TEMPLATE_CSS, $url . $file, $tag) . PHP_EOL;
            return $html;
        }

        /**
         * Utilizzo il template per ritornare la stringa html per gli script javascript
         *
         * @param $file
         * @param array $param
         * @return string
         */
        protected function renderAssetsJs($file, $param = [])
        {
            $html = '';
            $url = $this->getAssetsTheme();
            starts_with($file, ['http', 'https', '\\']) ? $url = null : null;
            starts_with($file, ['/']) ? $url = $this->getHttpAssetsTheme() : null;
            $tag = '';
            if (count($param)) {
                $totale = count($param);
                $n = 0;
                $tag .= ' ';
                foreach ($param as $key => $value) {
                    $tag .= $key . '="' . $value . '"';
                    $n++;
                    $n < $totale ? $tag .= ' ' : null;
                }
            }
            $html .= sprintf(self::TEMPLATE_JS, $url . $file, $tag) . PHP_EOL;
            return $html;
        }

        /**
         * @param $name
         * @param array $param
         * @return string
         */
        protected function renderAssetsJson($name, $param = [])
        {
            $json = $this->getJsonAssetsTheme();
            if (count($json)) {
                foreach ($json as $key => $value) {
                    if ($key == $name) {
                        if (is_array($value)) {
                            $file = $value[0];
                            if (count($value) > 1) {
                                $file = $value[0];
                                $param = $value[1];
                            }
                        } else {
                            $file = $value;
                        }
                        $ext = pathinfo($file, PATHINFO_EXTENSION);
                        switch ($ext) {
                            case 'js':
                                return $this->renderAssetsJs($file, $param);
                                break;
                            case 'css':
                                return $this->renderAssetsCss($file, $param);
                                break;
                        }
                    }
                }
            }
        }

        /**
         * Imposto dove cercare i layouts
         */
        protected function setPathLayouts()
        {
            $this->viewFactory->addLocation($this->getPathLayout());
        }

        /**
         * Imposto dove cercare le partials view
         */
        protected function setPathPartials()
        {
            $this->viewFactory->addLocation($this->getPathPartials());
        }

        /**
         * Ritorna una vista
         *
         * @param $view
         * @param array $param
         * @param string $layout
         * @return \Illuminate\Contracts\View\View
         */
        public function view($view, $param = [], $layout = null)
        {
            $this->setTheme();
            $this->setLayout($layout);
            $this->setPathLayouts();
            return $this->viewFactory->make($view, $param, ['theme_layout' => $this->layout]);
        }

        /**
         * Ritorna una vista parziale
         *
         * @param $view
         * @param array $param
         * @return string
         */
        public function partial($view, $param = [])
        {
            $this->setPathPartials();
            return $this->viewFactory->make($view, $param)->render();
        }

    }