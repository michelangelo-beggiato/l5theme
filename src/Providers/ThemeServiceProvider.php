<?php

    namespace Beggiatom\Theme\Providers;

    use Beggiatom\Theme\Theme;
    use Illuminate\Support\ServiceProvider;

    class ThemeServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap the application services.
         *
         * @return void
         */
        public function boot()
        {
            $this->publishes([
                __DIR__.'/../Config/theme.php' => config_path('theme.php'),
                __DIR__.'/../Vendor/themes' => public_path('themes')
            ]);

        }

        /**
         * Register the application services.
         *
         * @return void
         */
        public function register()
        {
            $this->app->bind('theme', function($app) {
                return new Theme($app['view'], $app['config'], $app['request']);
            });
        }

        public function provider() {
            return ['theme'];
        }

    }
