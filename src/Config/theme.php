<?php

    return [
        'default' => [
            'theme' => 'default',
            'layout' => 'default'
        ],
        'paths' => [
            'themes' => 'themes', // relative to public_path
            'layouts' => 'layouts', // relative to paths.themes.themeName
            'assets' => 'assets', // relative to paths.themes.themeName
            'partials' => 'partials', // relative to paths.themes.themeName.layouts
        ],
        'layouts' => [
            '/' => 'default',
            'home' => 'default',
        ]
    ];