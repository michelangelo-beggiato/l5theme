<?php

    namespace Beggiatom\Theme\Controllers;

    use App\Http\Controllers\Controller;
    use Theme;

    class ThemeController extends Controller
    {

        public function __construct() {

        }

        public function setTheme($theme) {
            Theme::setTheme($theme);
        }

        public function setLayout($layout = null) {
            Theme::setLayout($layout);
        }

        public function response($view, $param = [], $layout = null) {
            return Theme::view($view, $param, $layout);
        }

    }